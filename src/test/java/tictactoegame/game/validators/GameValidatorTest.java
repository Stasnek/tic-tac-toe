package tictactoegame.game.validators;

import org.testng.annotations.Test;
import tictactoegame.behaviorstrategies.LeatherBastardBehavior;
import tictactoegame.board.*;
import tictactoegame.game.GameMove;
import tictactoegame.game.Player;
import tictactoegame.game.validators.DefaultGameValidator;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class GameValidatorTest {

    private static final int BOARD_SIZE = 3;

    private final DefaultGameValidator validator = new DefaultGameValidator(false);
    private final Player player = new Player("test", CellMarker.X, new LeatherBastardBehavior());

    @Test
    public void testIsBoardFull() {
        Board board = new Board(BOARD_SIZE);

        assertFalse(validator.isBoardFull(board));

        for (int i = 1; i < BOARD_SIZE + 1; i++) {
            for (int j = 1; j < BOARD_SIZE + 1; j++) {
                board.addMoveOnBoard(new GameMove(new Coordinates(i, j), player));
            }
        }

        assertTrue(validator.isBoardFull(board));
    }

    @Test
    public void testHasSomeBodyWon() {
    }

    @Test
    public void testCheckRows() {
        Board board = new Board(BOARD_SIZE);
        Cell[][] cells = board.getCells();

        assertFalse(validator.checkRows(cells));

        cells[0][0].setOwner(player);
        cells[0][1].setOwner(player);
        cells[0][2].setOwner(player);
        assertTrue(validator.checkRows(cells));

        cells[0][0].setOwner(player);
        cells[0][1].setOwner(player);
        cells[0][2].setOwner(null);
        assertFalse(validator.checkRows(cells));

        cells[1][0].setOwner(player);
        cells[1][1].setOwner(player);
        cells[1][2].setOwner(player);
        assertTrue(validator.checkRows(cells));
    }

    @Test
    public void testCheckColumns() {
        Board board = new Board(BOARD_SIZE);
        Cell[][] cells = board.getCells();

        assertFalse(validator.checkColumns(cells));

        cells[0][0].setOwner(player);
        cells[1][0].setOwner(player);
        cells[2][0].setOwner(player);
        assertTrue(validator.checkColumns(cells));

        cells[0][0].setOwner(null);
        cells[1][0].setOwner(player);
        cells[2][0].setOwner(player);
        assertFalse(validator.checkColumns(cells));

        cells[0][2].setOwner(player);
        cells[1][2].setOwner(player);
        cells[2][2].setOwner(player);
        assertTrue(validator.checkColumns(cells));
    }

    @Test
    public void testCheckLeftRightDiagonal() {
        Board board = new Board(BOARD_SIZE);
        Cell[][] cells = board.getCells();

        assertFalse(validator.checkLeftRightDiagonal(cells));

        cells[0][0].setOwner(player);
        cells[1][1].setOwner(player);
        cells[2][2].setOwner(player);
        assertTrue(validator.checkLeftRightDiagonal(cells));

        cells[0][0].setOwner(null);
        cells[1][1].setOwner(player);
        cells[2][2].setOwner(player);
        assertFalse(validator.checkLeftRightDiagonal(cells));

        cells[0][0].setOwner(player);
        cells[1][1].setOwner(null);
        cells[2][2].setOwner(player);
        assertFalse(validator.checkLeftRightDiagonal(cells));

        cells[0][0].setOwner(player);
        cells[1][1].setOwner(player);
        cells[2][2].setOwner(null);
        assertFalse(validator.checkLeftRightDiagonal(cells));
    }

    @Test
    public void testCheckRightLeftDiagonal() {
        Board board = new Board(BOARD_SIZE);
        Cell[][] cells = board.getCells();

        assertFalse(validator.checkRightLeftDiagonal(cells));

        cells[0][2].setOwner(player);
        cells[1][1].setOwner(player);
        cells[2][0].setOwner(player);
        assertTrue(validator.checkRightLeftDiagonal(cells));

        cells[0][2].setOwner(null);
        cells[1][1].setOwner(player);
        cells[2][0].setOwner(player);
        assertFalse(validator.checkRightLeftDiagonal(cells));

        cells[0][2].setOwner(player);
        cells[1][1].setOwner(null);
        cells[2][0].setOwner(player);
        assertFalse(validator.checkRightLeftDiagonal(cells));

        cells[0][2].setOwner(player);
        cells[1][1].setOwner(player);
        cells[2][0].setOwner(null);
        assertFalse(validator.checkRightLeftDiagonal(cells));
    }
}