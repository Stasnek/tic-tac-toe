package tictactoegame.view;

import org.springframework.stereotype.Component;
import tictactoegame.game.GameMove;
import tictactoegame.board.Board;
import tictactoegame.game.Player;

@Component
public interface ViewController {
    void printGameState(Board board);

    GameMove processPlayerMove(Player player);
}
