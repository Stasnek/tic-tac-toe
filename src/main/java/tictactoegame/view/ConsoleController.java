package tictactoegame.view;

import dnl.utils.text.table.TextTable;
import org.springframework.stereotype.Component;
import tictactoegame.game.GameMove;
import tictactoegame.board.Board;
import tictactoegame.board.Cell;
import tictactoegame.board.Coordinates;
import tictactoegame.game.Player;

import java.util.Scanner;

@Component
public class ConsoleController implements ViewController {
    private Scanner scanner = new Scanner(System.in);

    public void printGameState(Board board) {
        String[] columnNames = getColumnNames(board);
        Cell[][] cells = board.getCells();
        Object[][] tableData = new Object[cells.length][cells[0].length];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                tableData[i][j] = cells[i][j].toString();
            }
        }
        TextTable textTable = new TextTable(columnNames, tableData);
        textTable.setAddRowNumbering(true);
        System.out.println("----------------------------------------------------");
        textTable.printTable();
    }


    public GameMove processPlayerMove(Player player) {
        System.out.println(player.getName() + ", It is your turn!");
        System.out.println("Input coordinates splitted by space!");
        String rawCoordinates = scanner.nextLine();
        String[] arrayCoordinates = rawCoordinates.split(" ");

        GameMove gameMove = new GameMove();
        if (arrayCoordinates.length != 2) {
            System.out.println("There must be two numbers!");
            gameMove.setCorrect(false);
            return gameMove;
        }

        int row = Integer.valueOf(arrayCoordinates[0]) - 1;
        int column = Integer.valueOf(arrayCoordinates[1]) - 1;

        gameMove.setCoordinates(new Coordinates(row, column));
        gameMove.setPlayer(player);
        return gameMove;
    }


    private String[] getColumnNames(Board board) {
        String[] columns = new String[board.getSideSize()];
        int sideSize = board.getSideSize();
        for (int i = 0; i < sideSize; i++) {
            columns[i] = String.valueOf(i + 1);
        }
        return columns;
    }
}
