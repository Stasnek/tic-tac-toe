package tictactoegame.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tictactoegame.board.Board;
import tictactoegame.game.validators.GameValidator;
import tictactoegame.view.ViewController;

import java.util.List;

@Component
public class Game {

    @Autowired
    private List<Player> players;

    @Autowired
    private ViewController viewController;

    @Autowired
    private GameSettings settings;

    @Autowired
    @Qualifier(value = "defaultValidator")
    private GameValidator validator;

    Logger logger = LogManager.getLogger(Game.class);

    private Board board;

    public void start() {
        if (players.size() < 2) {
            return;
        }

        board = new Board(settings.getSideSize());
        viewController.printGameState(board);

        boolean endOfGame = false;
        while (!endOfGame) {
            GameMove gameMove;
            for (Player player : players) {
                gameMove = processPlayerMove(player);
                viewController.printGameState(board);
                System.out.println(player.getName() + " has chosen "
                        + (gameMove.getCoordinates().getRow() + 1)
                        + " - " + (gameMove.getCoordinates().getColumn() + 1));
                System.out.println();
                if (endOfTheGame(player)) {
                    endOfGame = true;
                    break;
                }
            }
        }
    }

    private GameMove processPlayerMove(Player player) {
        GameMove gameMove;
        do {
            gameMove = player.getBehaviorStrategy()
                    .processMove(viewController, board, player);

        } while (!board.addMoveOnBoard(gameMove));
        return gameMove;
    }

    private boolean endOfTheGame(Player player) {
        if (validator.hasSomeBodyWon(board)) {
            System.out.println(player.getName() + " has won!");
            return true;
        }
        if (validator.isBoardFull(board)) {
            System.out.println("It is a draw!");
            return true;
        }
        return false;
    }


}
