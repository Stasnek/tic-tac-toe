package tictactoegame.game;

import tictactoegame.board.Coordinates;

import java.util.Objects;

public class GameMove {

    private Coordinates coordinates;
    private Player player;
    private boolean isCorrect = true;

    public GameMove() {
    }

    public GameMove(Coordinates coordinates, Player player) {
        this.coordinates = coordinates;
        this.player = player;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameMove move = (GameMove) o;
        return coordinates.equals(move.getCoordinates()) &&
                player.equals(move.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates, player);
    }
}
