package tictactoegame.game;

import tictactoegame.behaviorstrategies.BehaviorStrategy;
import tictactoegame.board.CellMarker;

import java.util.Objects;

public class Player {
    private String name;
    private CellMarker cellMarker;

    private BehaviorStrategy behaviorStrategy;

    public Player(String name, CellMarker cellMarker, BehaviorStrategy behaviorStrategy) {
        this.name = name;
        this.cellMarker = cellMarker;
        this.behaviorStrategy = behaviorStrategy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CellMarker getCellMarker() {
        return cellMarker;
    }

    public void setCellMarker(CellMarker cellMarker) {
        this.cellMarker = cellMarker;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return cellMarker == player.cellMarker;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cellMarker);
    }

    public BehaviorStrategy getBehaviorStrategy() {
        return behaviorStrategy;
    }

    public void setBehaviorStrategy(BehaviorStrategy behaviorStrategy) {
        this.behaviorStrategy = behaviorStrategy;
    }
}
