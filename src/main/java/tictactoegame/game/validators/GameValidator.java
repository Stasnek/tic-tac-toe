package tictactoegame.game.validators;

import tictactoegame.board.Board;

public interface GameValidator {
    boolean isBoardFull(Board board);

    boolean isBoardEmpty(Board board);

    boolean hasSomeBodyWon(Board board);
}
