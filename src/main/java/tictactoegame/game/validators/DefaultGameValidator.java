package tictactoegame.game.validators;

import tictactoegame.board.Board;
import tictactoegame.board.Cell;
import tictactoegame.game.Player;
import tictactoegame.utils.MathUtils;

public class DefaultGameValidator implements GameValidator {

    private boolean quite;

    public DefaultGameValidator(boolean quite) {
        this.quite = quite;
    }

    @Override
    public boolean isBoardFull(Board board) {
        for (Cell[] cellRow : board.getCells()) {
            for (Cell aCellRow : cellRow) {
                if (!aCellRow.isBusy()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean isBoardEmpty(Board board) {
        for (Cell[] cellRow : board.getCells()) {
            for (Cell aCellRow : cellRow) {
                if (aCellRow.isBusy()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean hasSomeBodyWon(Board board) {
        Cell[][] cells = board.getCells();
        return checkRows(cells) || checkColumns(cells)
                || checkLeftRightDiagonal(cells) || checkRightLeftDiagonal(cells);
    }


    boolean checkRows(Cell[][] cells) {
        boolean checkRows = checkMatrix(cells);
        if (!quite && checkRows) {
            System.out.println("Won by Row");
        }
        return checkRows;
    }

    boolean checkColumns(Cell[][] cells) {
        boolean checkColumns = checkMatrix(MathUtils.transposeMatrix(cells));
        if (!quite && checkColumns) {
            System.out.println("Won by Column");
        }
        return checkColumns;
    }


    boolean checkLeftRightDiagonal(Cell[][] cells) {
        Player player = cells[0][0].getOwner();

        if (player == null) {
            return false;
        }

        for (int i = 0; i < cells.length; i++) {
            if (cells[i][i].getOwner() != player) {
                return false;
            }
        }

        if (!quite) {
            System.out.println("Won by Left Right Diagonal");
        }
        return true;
    }

    boolean checkRightLeftDiagonal(Cell[][] cells) {
        Player player = cells[0][cells.length - 1].getOwner();

        if (player == null) {
            return false;
        }

        for (int i = 0; i < cells.length; i++) {
            if (cells[i][cells.length - 1 - i].getOwner() != player) {
                return false;
            }
        }

        if (!quite) {
            System.out.println("Won by Right Left Diagonal");
        }
        return true;
    }

    private boolean checkMatrix(Cell[][] cells) {
        for (Cell[] cell : cells) {
            if (checkArray(cell)) {
                return true;
            }
        }

        return false;
    }

    private boolean checkArray(Cell[] cells) {
        Player player = cells[0].getOwner();

        if (player == null) {
            return false;
        }

        for (Cell cell : cells) {
            if (cell.getOwner() != player) {
                return false;
            }
        }

        return true;
    }

    public boolean isQuite() {
        return quite;
    }

    public void setQuite(boolean quite) {
        this.quite = quite;
    }
}
