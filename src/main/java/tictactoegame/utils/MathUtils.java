package tictactoegame.utils;

import tictactoegame.board.Cell;

public final class MathUtils {
    private MathUtils() {

    }

    public static Cell[][] transposeMatrix(Cell[][] m) {
        Cell[][] temp = new Cell[m[0].length][m.length];

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = m[i][j];
            }
        }
        return temp;
    }
}
