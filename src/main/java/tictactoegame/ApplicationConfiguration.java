package tictactoegame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import tictactoegame.behaviorstrategies.BehaviorStrategy;
import tictactoegame.behaviorstrategies.LeatherBastardBehavior;
import tictactoegame.behaviorstrategies.TreeArtificialBehavior;
import tictactoegame.board.CellMarker;
import tictactoegame.game.Player;
import tictactoegame.game.validators.DefaultGameValidator;
import tictactoegame.game.validators.GameValidator;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("tictactoegame")
@PropertySource("classpath:game.properties")
public class ApplicationConfiguration {

    @Autowired
    private Environment env;

    @Bean(name = "players")
    List<Player> initPlayers() {
        List<Player> players = new ArrayList<>();
        players.add(initLeatherBastardPlayer());
        players.add(initAiPlayer());
        return players;
    }

    @Bean(name = "aiPlayer")
    @Order(value = 1)
    Player initAiPlayer() {
        return new Player(env.getProperty("ai.player.name")
                , CellMarker.O, initArtificialStrategy());
    }

    @Bean(name = "leatherBastardPlayer")
    @Order(value = 2)
    Player initLeatherBastardPlayer() {
        return new Player(env.getProperty("human.player.name")
                , CellMarker.X, initLeatherBastardBehaviorStrategy());
    }

    @Bean(name = "artificialBehavior")
    BehaviorStrategy initArtificialStrategy() {
        return new TreeArtificialBehavior();
    }

    @Bean(name = "leatherBastardBehavior")
    BehaviorStrategy initLeatherBastardBehaviorStrategy() {
        return new LeatherBastardBehavior();
    }

    @Bean(name = "defaultValidator")
    GameValidator initDefaultValidator() {
        return new DefaultGameValidator(false);
    }


    @Bean(name = "aiValidator")
    GameValidator initAiValidator() {
        return new DefaultGameValidator(true);
    }
}
