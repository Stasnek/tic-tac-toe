package tictactoegame.board;

import tictactoegame.game.GameMove;
import tictactoegame.game.Player;

import java.util.*;

public class Board {

    private int sideSize;
    private Cell[][] cells;
    private Set<Player> players;
    private List<GameMove> moves;

    public Board(int sideSize) {
        this.sideSize = sideSize;
        this.cells = new Cell[sideSize][sideSize];
        players = new HashSet<>();
        moves = new ArrayList<>();
        initBoard();
    }

    public boolean addMoveOnBoard(GameMove move) {
        if (!move.isCorrect()) {
            return false;
        }

        Coordinates coordinates = move.getCoordinates();
        int row = coordinates.getRow();
        int column = coordinates.getColumn();
        if (row > sideSize - 1 || column > sideSize - 1
                || row < 0 || column < 0) {
            System.out.println("Coordinates are out of the board!");
            return false;
        }
        if (cells[row][column].isBusy()) {
            System.out.println("Current cell is already taken!");
            return false;
        }
        cells[row][column].setOwner(move.getPlayer());
        players.add(move.getPlayer());
        moves.add(move);
        return true;
    }

    public List<Coordinates> getAvailAbleCells() {
        List<Coordinates> availableCells = new ArrayList<>();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                if (!cells[i][j].isBusy()) {
                    availableCells.add(new Coordinates(i, j));
                }
            }
        }
        return availableCells;
    }


    private void initBoard() {
//        Arrays.stream(cells)
//                .forEach(row -> Arrays.stream(row)
//                        .forEach(cell -> cell = new Cell()));
        for (Cell[] cellRow : cells) {
            for (int i = 0; i < cellRow.length; i++) {
                cellRow[i] = new Cell();
            }
        }
    }

    public Board clone() {
        Board newBoard = new Board(this.getSideSize());
        Cell[][] newCells = new Cell[this.getSideSize()][this.getSideSize()];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                newCells[i][j] = cells[i][j].clone();
            }
        }
        newBoard.setCells(newCells);
        newBoard.setMoves(new ArrayList<>(moves));
        return newBoard;
    }

    public int getSideSize() {
        return sideSize;
    }

    public void setSideSize(int sideSize) {
        this.sideSize = sideSize;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(14);
        for (int i = 0; i < cells.length; i++) {
            builder.append("[");
            for (int j = 0; j < cells.length; j++) {
                builder.append(cells[j][i]);
                if (j == 0) {
                    builder.append(" , ");
                } else if (j < cells.length - 1) {
                    builder.append(", ");
                } else {
                    builder.append("]");
                }

            }
        }
        return builder.toString();

    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public List<GameMove> getMoves() {
        return moves;
    }

    public void setMoves(List<GameMove> moves) {
        this.moves = moves;
    }

    public GameMove getLastMove() {
        return moves.get(moves.size() - 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board = (Board) o;
        int index = 0;
        for (Cell[] cell : cells) {
            if (!Arrays.equals(cell, board.getCells()[index])) {
                return false;
            }
            index++;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(sideSize, players, moves);
        result = 31 * result + Arrays.hashCode(cells);
        return result;
    }

    public boolean isEmpty() {
        return moves.isEmpty();
    }
}
