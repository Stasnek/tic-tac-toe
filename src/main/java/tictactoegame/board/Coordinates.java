package tictactoegame.board;

import java.util.Objects;

public class Coordinates {
    private int row;
    private int column;

    public Coordinates(int row, int column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return row == that.row &&
                column == that.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }

    public int getRow() {
        return row;
    }

    public void setRow(int xAxis) {
        this.row = xAxis;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int yAxis) {
        this.column = yAxis;
    }

    @Override
    public String toString() {
        return row + "," + column;
    }
}
