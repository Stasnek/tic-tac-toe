package tictactoegame.board;

public enum CellMarker {

    O {
        @Override
        public String toString() {
            return "O";
        }
    },
    X {
        @Override
        public String toString() {
            return "X";
        }
    },
    T {
        @Override
        public String toString() {
            return "T";
        }
    },

    EMPTY {
        @Override
        public String toString() {
            return "";
        }
    }
}
