package tictactoegame.board;

import tictactoegame.game.Player;

import java.util.Objects;

public class Cell {
    private Player owner;

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public boolean isBusy() {
        return owner != null;
    }

    @Override
    public String toString() {
        return owner == null
                ? CellMarker.EMPTY.toString()
                : owner.getCellMarker().toString();
    }

    public Cell clone() {
        Cell cell = new Cell();
        cell.setOwner(this.getOwner());
        return cell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(owner, cell.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner);
    }
}
