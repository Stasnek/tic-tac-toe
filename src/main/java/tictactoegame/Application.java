package tictactoegame;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import tictactoegame.game.Game;

@Component
public class Application {

    public static void main(String[] args) {
        new Application().run();
    }

    private void run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        Game game = context.getBean(Game.class);
        game.start();
    }
}
