package tictactoegame.behaviorstrategies;

import tictactoegame.game.GameMove;
import tictactoegame.board.Board;
import tictactoegame.board.Coordinates;
import tictactoegame.game.Player;
import tictactoegame.view.ViewController;

import java.util.List;
import java.util.Random;

public class RandomArtificialBehavior implements BehaviorStrategy {

    private Random random = new Random();

    public GameMove processMove(ViewController viewController, Board board, Player player) {
        List<Coordinates> availAbleCells = board.getAvailAbleCells();
        int index = random.nextInt(availAbleCells.size());
        Coordinates chosenCell = availAbleCells.get(index);
        int row = chosenCell.getRow();
        int column = chosenCell.getColumn();
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            //No-op
        }
        return new GameMove(new Coordinates(row, column), player);
    }

    public void killHumanity() {
        System.out.println("Leather bastards! You gonna die!");
    }


}
