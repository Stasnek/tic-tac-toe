package tictactoegame.behaviorstrategies;

import tictactoegame.game.GameMove;
import tictactoegame.board.Board;
import tictactoegame.game.Player;
import tictactoegame.view.ViewController;

public interface BehaviorStrategy {

    GameMove processMove(ViewController viewController, Board board, Player player);
}
