package tictactoegame.behaviorstrategies;

import org.springframework.beans.factory.annotation.Autowired;
import tictactoegame.board.Board;
import tictactoegame.board.Coordinates;
import tictactoegame.game.GameMove;
import tictactoegame.game.Player;
import tictactoegame.gametree.GameState;
import tictactoegame.gametree.GameTree;
import tictactoegame.gametree.Node;
import tictactoegame.gametree.TreeBuilder;
import tictactoegame.view.ViewController;

import java.util.List;
import java.util.Optional;

public class TreeArtificialBehavior implements BehaviorStrategy {

    private static final int MAX_DEEP = 2;
    private GameTree tree;
    private Node currentNode;

    @Autowired
    private TreeBuilder treeBuilder;

    @Override
    public GameMove processMove(ViewController viewController, Board board, Player player) {

        sleep();

        if (tree == null) {
            Node node = new Node();
            tree = new GameTree(node);

            if (board.isEmpty()) {
                int half = board.getSideSize() / 2;
                GameMove move = new GameMove(new Coordinates(half, half), player);
                node.setMove(move);
                Board clone = board.clone();
                clone.addMoveOnBoard(move);
                treeBuilder.buildTree(node, clone, player, false, MAX_DEEP);
                currentNode = node;
                return move;
            } else {
                treeBuilder.buildTree(node, board, player, true, MAX_DEEP);
                currentNode = makeMove(node);
            }
        } else {
            currentNode = updateCurrentNode(currentNode, board, player);
            currentNode = makeMove(currentNode);
        }

        return currentNode.getMove();
    }


    private Node makeMove(Node node) {
        Node maxWinningNode = null;
        int maxWiningStates = Integer.MIN_VALUE;
        boolean allChildrenContainsLose = false;
        List<Node> children = node.getChildren();

        allChildrenContainsLose = children
                .stream()
                .filter(Node::containsLoseInChildren)
                .count() == children.size();

        for (Node child : children) {

            if (child.getGameState().equals(GameState.WIN)) {
                return child;
            }

            int winingStates = calculateWiningStates(child);

            if (allChildrenContainsLose || !child.containsLoseInChildren()) {
                if (winingStates > maxWiningStates) {
                    maxWinningNode = child;
                    maxWiningStates = winingStates;
                }
            }

        }

        if (maxWinningNode == null) {
            System.out.println("maxWiningStates = " + maxWiningStates);
        }
        return maxWinningNode;
    }

    private int calculateWiningStates(Node node) {
        int winingStates = 0;
        for (Node child : node.getChildren()) {
            if (child.getGameState().equals(GameState.WIN)) {
                winingStates++;
            } else if (child.getGameState().equals(GameState.IN_PROGRESS)) {
                winingStates += calculateWiningStates(child);
            }

        }
        return winingStates;
    }

    private Node updateCurrentNode(Node node, Board board, Player ai) {
        if (node.getChildren().isEmpty()) {
            treeBuilder.buildTree(node, board, ai, true, MAX_DEEP);
        }

        GameMove lastMove = board.getLastMove();
        Optional<Node> optional = node.getChildren()
                .stream()
                .filter(n -> n.getMove().equals(lastMove))
                .findFirst();

        if (optional.isPresent()) {
            Node actualNode = optional.get();
            if (actualNode.getChildren().isEmpty()) {
                treeBuilder.buildTree(actualNode, board, ai, true, MAX_DEEP);
            }
            return actualNode;
        }


        throw new RuntimeException("Ooops! Node not found.");
    }

    private void sleep() {
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            //No-op
        }
    }
}
