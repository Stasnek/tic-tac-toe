package tictactoegame.behaviorstrategies;

import tictactoegame.game.GameMove;
import tictactoegame.board.Board;
import tictactoegame.game.Player;
import tictactoegame.view.ViewController;

public class LeatherBastardBehavior implements BehaviorStrategy {

    @Override
    public GameMove processMove(ViewController viewController, Board board, Player player) {
        return viewController.processPlayerMove(player);
    }
}
