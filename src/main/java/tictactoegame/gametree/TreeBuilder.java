package tictactoegame.gametree;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tictactoegame.board.Board;
import tictactoegame.board.Coordinates;
import tictactoegame.game.GameMove;
import tictactoegame.game.Player;
import tictactoegame.game.validators.GameValidator;

import java.util.List;

@Component
public class TreeBuilder {

    @Autowired
    @Qualifier(value = "aiValidator")
    private GameValidator validator;

    @Autowired
    @Qualifier("leatherBastardPlayer")
    private Player opponent;
//
//    @Autowired
//    @Qualifier("aiPlayer")
//    private Player ai;

    public void buildTree(Node root, Board board, Player ai, boolean aiTurn, int maxDeep) {
        root.setGameState(GameState.IN_PROGRESS);
        processLayer(root, board, ai, aiTurn, 1, maxDeep);
    }

    private void processLayer(Node parent, Board board, Player ai, boolean aiTurn, int currentDeep, int maxDeep) {
        List<Coordinates> availAbleCells = board.getAvailAbleCells();
        if (!availAbleCells.isEmpty()) {
            Player player = aiTurn ? ai : opponent;
            for (Coordinates availableCell : availAbleCells) {
                GameMove move = new GameMove(availableCell, player);
                Board tempBoard = board.clone();
                tempBoard.addMoveOnBoard(move);
                Node node = new Node(parent);
                node.setMove(move);
                parent.addChild(node);
//                System.out.println("currentDeep = " + currentDeep + " board - " + tempBoard);
                setGameState(aiTurn, tempBoard, node);

                if (currentDeep < maxDeep) {
                    processLayer(node, tempBoard, ai, !aiTurn, currentDeep + 1, maxDeep);
                }
            }
        }
    }

    private void setGameState(boolean aiTurn, Board tempBoard, Node node) {
        if (validator.hasSomeBodyWon(tempBoard)) {
            node.setGameState(aiTurn ? GameState.WIN : GameState.LOSE);
        } else if (validator.isBoardFull(tempBoard)) {
            node.setGameState(GameState.DRAW);
        } else {
            node.setGameState(GameState.IN_PROGRESS);
        }
    }
}
