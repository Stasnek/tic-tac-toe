package tictactoegame.gametree;

import tictactoegame.game.GameMove;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private Node parent;
    private List<Node> children;
    private GameState gameState;
    private GameTree tree;
    private GameMove move;

    public Node() {
        children = new ArrayList<>();
//        this.tree = tree;
    }

    public Node(Node parent) {
        this.parent = parent;
        tree = parent.getTree();
        children = new ArrayList<>();
    }

    public void addChild(Node node) {
        children.add(node);
        tree.increaseNodesCount();
    }

    public boolean containsLoseInChildren() {
        return this.getChildren().stream().anyMatch(n -> n.getGameState().equals(GameState.LOSE));
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
        tree.addGameStateToMap(gameState);
    }

    public GameTree getTree() {
        return tree;
    }

    public void setTree(GameTree tree) {
        this.tree = tree;
    }

    public GameMove getMove() {
        return move;
    }

    public void setMove(GameMove move) {
        this.move = move;
    }

    @Override
    public String toString() {
        return "Node{" +
                "gameState=" + gameState +
                '}';
    }
}
