package tictactoegame.gametree;

import java.util.HashMap;
import java.util.Map;

public class GameTree {
    private Node root;
    private int nodeCount;
    private Map<GameState, Integer> states = new HashMap<>();

    public GameTree(Node root) {
        this.root = root;
        this.root.setTree(this);
        states.put(GameState.IN_PROGRESS, 1);
        states.put(GameState.DRAW, 0);
        states.put(GameState.WIN, 0);
        states.put(GameState.LOSE, 0);
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public void increaseNodesCount() {
        nodeCount++;
    }

    public void addGameStateToMap(GameState state) {
        Integer count = states.get(state);
        states.put(state, ++count);
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public Map<GameState, Integer> getStates() {
        return states;
    }

    public void setStates(Map<GameState, Integer> states) {
        this.states = states;
    }
}
