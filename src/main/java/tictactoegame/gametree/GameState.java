package tictactoegame.gametree;

public enum GameState {
    IN_PROGRESS, DRAW, WIN, LOSE
}
